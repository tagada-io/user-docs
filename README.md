# Tagada user documentation and blog

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern
static website generator.

### Installation

You'll need nodejs 16.x and npm

### Local Development

In order to test your changes, you can run the following commands:

```bash
# Build your changes locally
$ npm run build

# Run a local web server to see your changes
# http://localhost:3000
$ npm run serve
```

The last command starts a local development server and opens up a browser window.
Most changes are reflected live without having to restart the server.
