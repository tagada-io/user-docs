---
slug: yubikey-as-gpg-smart-card
title: Your Yubikey as a GPG smart card
authors: [matt]
tags: [security]
description: Need to use your GPG key. Store it on your Yubikey and use it as a smart card.
---

Yubikey is a hardware authentication device manufactured by [Yubico][l1]. The
latests key (Yubikey 5 NFC) supports One Time Passwords (OTP), public key
cryptography, Universal 2nd Factor (U2F) and more. It has several advantages
such as dust and water proof, doesn't require a battery. It has several
interesting competitors such as the [Nitrokey][l2], the [Thethis][l3] key or
the [Google Titan][l4]. All the products mentionned above are interesting with
pros and cons. Yubikey seems to offer the best features related to the price.
If you wish to dig up a bit, Benjamin from Infosec Handbook wrote an
interesting comparison article about [Yubikey and Nitrokey][l5].
<!--truncate-->

Now, time to use our device as our GPG key to encrypt or sign files, emails,
commits, etc. The main advantage of doing that being that your private key
won't stay on your computer. This article will suppose that you have [created
a GPG key pair][l6].

## Smart card configuration

First things first, you imported your GPG secret key:

```bash
# Import your GPG secret key
gpg --import </path/to/your/secret/key.asc>

# List the secret keys to ensure that yours has been correctly imported
gpg --list-secret-keys
```

You should now plug in your Yubikey and configure it as a smartcard:

```bash
# Check if your yubikey is recognized
gpg --card-status

# Edit the card
gpg --card-edit

Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240103040006142519700000
Application type .: OpenPGP
Version ..........: 3.4
Manufacturer .....: Yubico
Serial number ....: 123456789
Name of cardholder: [not set]
Language prefs ...: [not set]
Salutation .......:
URL of public key : [not set]
Login data .......: [not set]
Signature PIN ....: not forced
Key attributes ...: rsa4096 rsa4096 rsa4096
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 0 3
Signature counter : 0
KDF setting ......: off
Signature key ....: [none]
Encryption key....: [none]
Authentication key: [none]
General key info..: [none]

gpg/card>
```

I would recommend first to change both admin and user pin codes. The default
user pin is `123456` and admin pin (PUK) is `12345678`. The first thing would
be to change the admin PIN then the user PIN.

```bash
gpg/card> admin
Admin commands are allowed

gpg/card> passwd
gpg: OpenPGP card no. D2760001240103040006142519700000 detected

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 3
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? 1
PIN changed.

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

Your selection? q
```

You can eventually edit some optional fields such as your language preferences,
your login, last and first name, etc. To know how to edit these fields you
just need to enter the help command at the `gpg/card>` prompt.

## Transfer the keys onto the smartcard

:::danger
Transfering your keys to the Yubikey is a destructive operation, do
not forget to backup your keys before starting this part.
:::

Keep in mind that the key you'll work on will be indicated by a star `*`.

```bash
gpg --edit-key <KEY_ID>

Secret key is available.

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: C
    trust: ultimate      validity: ultimate
ssb  rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: 2018-10-09  usage: S
ssb  rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: 2018-10-09  usage: E
ssb  rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: 2018-10-09  usage: A
[ultimate] (1). Mr Nobody <nobody@example.net>
```

You'll need to transfer the signin, encryption and authentication key to the
smartcard and select them one by one. Do not forget to unselect the previous
key before moving on to the next.

```bash
gpg> key 1

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: C
    trust: ultimate      validity: ultimate
ssb* rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: 2018-10-09  usage: S
ssb  rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: 2018-10-09  usage: E
ssb  rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: 2018-10-09  usage: A
[ultimate] (1). Mr Nobody <nobody@example.net>

gpg> keytocard
Please select where to store the key:
   (1) Signature key
   (3) Authentication key
Your selection? 1
```

You'll be asked to enter both your GPG passphrase as well as your user PIN
number to perform the operation.

```bash
gpg> key 1

gpg> key 2

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: C
    trust: ultimate      validity: ultimate
ssb  rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: 2018-10-09  usage: S
ssb* rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: 2018-10-09  usage: E
ssb  rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: 2018-10-09  usage: A
[ultimate] (1). Mr Nobody <nobody@example.net>

gpg> keytocard
Please select where to store the key:
   (2) Encryption key
Your selection? 2
```

```bash
gpg> key 2

gpg> key 3

sec  rsa4096/0xFF3E7D88647EBCDB
    created: 2017-10-09  expires: never       usage: C
    trust: ultimate      validity: ultimate
ssb  rsa4096/0xBECFA3C1AE191D15
    created: 2017-10-09  expires: 2018-10-09  usage: S
ssb  rsa4096/0x5912A795E90DD2CF
    created: 2017-10-09  expires: 2018-10-09  usage: E
ssb* rsa4096/0x3F29127E79649A3D
    created: 2017-10-09  expires: 2018-10-09  usage: A
[ultimate] (1). Mr Nobody <nobody@example.net>

gpg> keytocard
Please select where to store the key:
   (3) Authentication key
Your selection? 3
```

All keys are transfered, don't forget to save before quitting the GPG edition
mode:

```bash
gpg> save
```

In order to verify that everything worked as planned, just use the command `gpg
-K` that will list the secret keys. Observe that now instead of reading `ssb
[...]` you will see `ssb> [...]` which means that your keys are no longer on
your drive.

```bash
gpg -K
/tmp.FLZC0xcM/pubring.kbx
-------------------------------------------------------------------------
sec   rsa4096/0xFF3E7D88647EBCDB 2017-10-09 [C]
      Key fingerprint = 011C E16B D45B 27A5 5BA8  776D FF3E 7D88 647E BCDB
uid                            Mr Nobody <nobody@example.net>
ssb>  rsa4096/0xBECFA3C1AE191D15 2017-10-09 [S] [expires: 2018-10-09]
ssb>  rsa4096/0x5912A795E90DD2CF 2017-10-09 [E] [expires: 2018-10-09]
ssb>  rsa4096/0x3F29127E79649A3D 2017-10-09 [A] [expires: 2018-10-09]
```

If you have several keys (such as a backup key in case the main one isn't
usable anymore), you'll have to reproduce all the operation above and your
public and secrets keys from your GPG keyring first before re-importing them.

Once everything is done, you can cleanup again, remove and re-insert the
Yubikey and check the result:

```bash
gpg --card-status
Reader ...........: Yubico YubiKey OTP FIDO CCID 00 00
Application ID ...: D2760001240102010006055532110000
Version ..........: 3.4
Manufacturer .....: Yubico
Serial number ....: 123456
Name of cardholder: Mr Nobody
Language prefs ...: en
Sex ..............: unspecified
URL of public key : [not set]
Login data .......: nobody@example.net
Signature PIN ....: not forced
Key attributes ...: rsa4096 rsa4096 rsa4096
Max. PIN lengths .: 127 127 127
PIN retry counter : 3 3 3
Signature counter : 0
Signature key ....: 07AA 7735 E502 C5EB E09E  B8B0 BECF A3C1 AE19 1D15
      created ....: 2016-05-24 23:22:01
Encryption key....: 6F26 6F46 845B BEB8 BDF3  7E9B 5912 A795 E90D D2CF
      created ....: 2016-05-24 23:29:03
Authentication key: 82BE 7837 6A3F 2E7B E556  5E35 3F29 127E 7964 9A3D
      created ....: 2016-05-24 23:36:40
General key info..: pub  4096R/0xBECFA3C1AE191D15 2016-05-24 Mr Nobody <nobody@example.net>
sec#  4096R/0xFF3E7D88647EBCDB  created: 2016-05-24  expires: never
ssb>  4096R/0xBECFA3C1AE191D15  created: 2017-10-09  expires: 2018-10-09
                      card-no: 0006 05553211
ssb>  4096R/0x5912A795E90DD2CF  created: 2017-10-09  expires: 2018-10-09
                      card-no: 0006 05553211
ssb>  4096R/0x3F29127E79649A3D  created: 2017-10-09  expires: 2018-10-09
                      card-no: 0006 05553211
```

## Some tests

You can now try to encrypt and decrypt a file to see if it works:

```bash
# Text encryption
echo "test message string" | gpg --encrypt --armor --recipient <KEY_ID> -o encrypted.txt
```

And now decrypt it:

```bash
gpg --decrypt --armor encrypted.txt
gpg: anonymous recipient; trying secret key 0x0000000000000000 ...
gpg: okay, we are the anonymous recipient.
gpg: encrypted with RSA key, ID 0x0000000000000000
test message string
```

For those who uses a password manager such as [Pass][l7], you should be able
now to try to check any data stored in it and be asked to enter the user PIN
number to access the data.

```bash
pass -c something/stored/pass
```

[l1]: https://www.yubico.com/
[l2]: https://www.nitrokey.com/
[l3]: https://thetis.io/
[l4]: https://cloud.google.com/titan-security-key/
[l5]: https://infosec-handbook.eu/blog/yubikey4c-nitrokeypro/
[l6]: https://alexcabal.com/creating-the-perfect-gpg-keypair
[l7]: https://www.passwordstore.org/
